from textures.texture import Texture

for tb_color in ['red','yellow','white','blue','brown'] :
    facades.append(Texture('tex.src/timber_framing_' + tb_color + '_20x24m.png',
        h_size_meters=20.0, h_cuts=[0, 71, 227, 302, 374, 526, 601, 671, 826, 898, 974, 1131, 1203, 1200], h_can_repeat=True,
        v_size_meters=24.0, v_cuts=[1, 99, 260, 413, 590, 749, 808, 972, 1074, 1129, 1290, 1292], v_can_repeat=False,
        v_align_bottom=True, height_min=0,
        requires=[],
        provides=['building:material:timber_framing','building:colour:'+tb_color, 'age:old', 'shape:urban', 'compat:roof-pitched']))
        
# ~ pink        
facades.append(Texture('tex.src/timber_framing_pink_20x24m.png',
        h_size_meters=20.0, h_cuts=[0, 71, 227, 302, 374, 526, 601, 671, 826, 898, 974, 1131, 1203, 1200], h_can_repeat=True,
        v_size_meters=24.0, v_cuts=[1, 99, 260, 413, 590, 749, 808, 972, 1074, 1129, 1290, 1292], v_can_repeat=False,
        v_align_bottom=True, height_min=0,
        requires=[],
        provides=['building:material:timber_framing','building:colour:pink', 'building:colour:purple', 'building:colour:fushia', 'age:old', 'shape:urban', 'compat:roof-pitched']))

# ~ cream
facades.append(Texture('tex.src/timber_framing_cream_20x24m.png',
        h_size_meters=20.0, h_cuts=[0, 71, 227, 302, 374, 526, 601, 671, 826, 898, 974, 1131, 1203, 1200], h_can_repeat=True,
        v_size_meters=24.0, v_cuts=[1, 99, 260, 413, 590, 749, 808, 972, 1074, 1129, 1290, 1292], v_can_repeat=False,
        v_align_bottom=True, height_min=0,
        requires=[],
        provides=['building:material:timber_framing','building:colour:cream', 'building:colour:antiquewhite', 'age:old', 'shape:urban', 'compat:roof-pitched']))