from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/residential/facade_modern36x36_12.png',
    36., [], True,
    36., [158, 234, 312, 388, 465, 542, 619, 697, 773, 870, 1024], False,
    height_min=20,
    provides=['shape:urban','shape:residential','age:modern',
                'compat:roof-flat']))

