
from textures.texture import Texture

facades.append(Texture('tex.src/de/residential/hotel_18x40m.png',
    h_size_meters=17.7, h_cuts=[1250], h_can_repeat=True,
    v_size_meters=40.0, v_cuts=[173, 378, 595, 805, 1018, 1227, 1437, 1658, 1855, 2057, 2310, 2818], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=[]))
