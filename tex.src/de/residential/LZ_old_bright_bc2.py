from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/residential/LZ_old_bright_bc2.png',
    17.9, [345,807,1023,1236,1452,1686,2048], True,
    14.8, [558,1005,1446,2048], False,
    provides=['shape:residential','age:old','compat:roof-flat','compat:roof-pitched']))
