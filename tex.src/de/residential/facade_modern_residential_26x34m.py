from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/residential/facade_modern_residential_26x34m.jpg',
    26.3, [429, 1723, 2142], True,
    33.9, [429, 666, 919, 1167, 1415, 1660, 1905, 2145, 2761], False,
    v_align_bottom = True, height_min=20,
    provides=['shape:urban','shape:residential','age:modern',
                'compat:roof-flat']))
