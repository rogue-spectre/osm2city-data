from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/residential/DSCF9710.png',
    29.9, [142,278,437,590,756,890,1024], True,
    19.8, [130,216,297,387,512], False,
    provides=['shape:residential','age:old','compat:roof-flat','compat:roof-pitched']))

