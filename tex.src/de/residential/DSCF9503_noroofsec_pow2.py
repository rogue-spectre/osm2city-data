from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/residential/DSCF9503_noroofsec_pow2.png',
    12.85, [360, 708, 1044, 1392, 2048], True,
    17.66, [556,1015,1474,2048], False,
    requires=['roof:colour:black'],
    provides=['shape:residential','age:old','compat:roof-flat','compat:roof-pitched']))

