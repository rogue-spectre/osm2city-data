
from textures.texture import Texture

facades.append(Texture('tex.src/de/residential/cream3_10x06m.png',
    h_size_meters=9.7, h_cuts=[681, 1163, 1203, 1233, 1268, 1304, 1351], h_can_repeat=True,
    v_size_meters=6.4, v_cuts=[351, 758, 900], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
