from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/residential/DSCF9678_pow2.png',
    10.4, [97,152,210,299,355,411,512], True,
    15.5, [132,211,310,512], False,
    provides=['shape:residential','shape:commercial','age:modern','compat:roof-flat']))
