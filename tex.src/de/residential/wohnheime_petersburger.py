from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/residential/wohnheime_petersburger.png',
    15.6, [215, 414, 614, 814, 1024], False,
    15.6, [112, 295, 477, 660, 843, 1024], True,
    height_min = 15.,
    provides=['shape:urban', 'shape:residential', 'age:modern',
                'compat:roof-flat']))
