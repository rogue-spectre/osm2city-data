from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/residential/DSCF9726_noroofsec_pow2.png',
    15.1, [321,703,1024], True,
    9.6, [227,512], False,
    provides=['shape:residential','age:old','compat:roof-flat','compat:roof-pitched']))
