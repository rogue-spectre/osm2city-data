from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/residential/DSCF9495_pow2.png',
    14, [585, 873, 1179, 1480, 2048], True,
    19.4, [274, 676, 1114, 1542, 2048], False,
    height_max = 13.,
    v_align_bottom = True,
    requires=['roof:colour:black'],
    provides=['shape:residential','age:old','compat:roof-flat','compat:roof-pitched']))
