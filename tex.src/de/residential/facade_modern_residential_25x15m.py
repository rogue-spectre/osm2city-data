from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/residential/facade_modern_residential_25x15m.jpg',
    25.0, [436, 1194, 2121, 2809, 3536], True,
    14.8, [718, 2096], False,
    v_align_bottom = True,
    requires=['roof:colour:black'],
    provides=['shape:residential','age:modern','compat:roof-flat']))
