from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/industrial/facade_industrial_white_26x14m.jpg',
    25.7, [165, 368, 575, 781, 987, 1191, 1332], True,
    13.5, [383, 444, 501, 562, 621, 702], False,
    v_align_bottom = True,
    requires=[],
    provides=['shape:industrial','age:modern', 'compat:roof-flat']))
