from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/industrial/facade_industrial_red_white_24x18m.jpg',
    23.8, [364, 742, 1086], True,
    18.5, [295, 565, 842], False,
    v_align_bottom = True,
    requires=[],
    provides=['shape:industrial','age:old', 'compat:roof-flat','compat:roof-pitched']))
