
from textures.texture import Texture

facades.append(Texture('tex.src/de/industrial/old_factory_30x07m.png',
    h_size_meters=30.0, h_cuts=[1108, 1993, 2866, 3783, 4701, 5733], h_can_repeat=True,
    v_size_meters=6.6, v_cuts=[745, 1452], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:industrial', 'age:old', 'age:modern', 'compat:roof-flat']))
