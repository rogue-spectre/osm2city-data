from textures.texture import Texture

facades.append(Texture('tex.src/de/commercial/facade_modern_commercial_red_gray_20x14m.jpg',
    20.0, [588, 1169, 1750, 2327, 2911, 3485], True,
    14.1, [755, 1289, 1823, 2453], False,
    v_align_bottom = True,
    requires=['roof:colour:black'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))
