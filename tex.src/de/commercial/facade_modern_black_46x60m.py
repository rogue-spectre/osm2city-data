from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/commercial/facade_modern_black_46x60m.jpg',
    45.9, [167, 345, 521, 700, 873, 944], True,
    60.5, [144, 229, 311, 393, 480, 562, 645, 732, 818, 901, 983, 1067, 1154, 1245], False,
    v_align_bottom = True, height_min=20.,
    requires=['roof:colour:black'], #, 'roof:color:gray'],
    provides=['shape:urban','age:modern', 'compat:roof-flat']))
