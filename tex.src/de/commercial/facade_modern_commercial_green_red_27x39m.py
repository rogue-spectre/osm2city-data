from textures.texture import Texture

facades.append(Texture('tex.src/de/commercial/facade_modern_commercial_green_red_27x39m.jpg',
    27.3, [486, 944, 1398, 1765, 2344], True,
    38.9, [338, 582, 839, 1087, 1340, 1593, 1856, 2094, 3340], False,
    v_align_bottom = True,
    requires=['roof:colour:black'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))
