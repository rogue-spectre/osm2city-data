from textures.texture import Texture

facades.append(Texture('tex.src/de/commercial/facade_modern_21x42m.jpg',
    20.6, [40, 79, 115, 156, 196, 235, 273, 312, 351, 389, 428, 468, 507, 545, 584, 624, 662], True,
    42.0, [667, 597, 530, 460, 391, 322, 254, 185, 117, 48, 736, 804, 873, 943, 1012, 1080, 1151, 1218, 1288, 1350], False, True,
    v_align_bottom = True,
    requires=[],
    provides=['shape:urban', 'shape:industrial', 'age:old', 'age:modern', 'compat:roof-flat']))
