from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/de/commercial/facade_modern_commercial_35x20m.jpg',
    34.6, [105, 210, 312, 417, 519, 622, 726, 829, 933, 1039, 1144, 1245, 1350], True,
    20.4, [177, 331, 489, 651, 796], False,
    v_align_bottom = True,
    requires=['roof:colour:black'], #, 'roof:color:gray'],
    provides=['shape:commercial','age:modern', 'compat:roof-flat']))

