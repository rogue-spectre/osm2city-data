from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/us/commercial/50storySteelGlassmodern1.jpg',
    h_size_meters=41, h_cuts=[], h_can_repeat=False,
    v_size_meters=165, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:gray'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))
