from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/us/commercial/25storyBrownWide1.jpg',
    h_cuts=[], h_can_repeat=False,
    v_cuts=[], v_can_repeat=False,
    levels=20,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:gray'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))
