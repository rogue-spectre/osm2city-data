from textures.texture import Texture

facades.append(Texture(tex_prefix + 'tex.src/us/commercial/US-dcofficeconcrwhite6-7st.jpg',
    h_cuts=[], h_can_repeat=False,
    levels=7, v_cuts=[], v_can_repeat=False,
    v_align_bottom=True, height_min=10,
    requires=['roof:colour:gray'],
    provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))
