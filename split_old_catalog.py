#!/usr/bin/env python2
"""
split old catalog entry for textures into individual .py snippets
"""

import numpy as np
import sys
import os
f = open('src.txt')

lines = []
for line in f.readlines():
    if line.strip().startswith('#'): continue
    lines.append(line)
f.close()
i = 0
# skip until start of facade
try:
    while 1:
        print "---- next "
        write = True
        while lines[i].strip() == '':
            i += 1

        path = lines[i].split("'")[1]
        if os.path.exists(path):
            pass
        else:
            print "  NO image", path
            write = False

        rest = []
        rest.append(lines[i])
        i += 1
        while lines[i].strip() != '':
            rest.append(lines[i])
            i += 1

        if not write:
            continue

        filename, ext = os.path.splitext(path)
        py = filename + '.py'
        if os.path.exists(py):
            print "  py exists", path
            continue

        out = open(py, 'w')
        out.write('from textures.texture import Texture\n\n')
        for l in rest:
            out.write(l)
        out.close()

except IndexError:
    pass
